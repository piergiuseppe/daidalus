# DAIDALUS Connector

**For a detailed description download "REPORT_DAIDALUS_Connector.pdf" in the root folder.**

## Importing WorldWind in Eclipse

1. Download NASA Worldwind: http://worldwind.arc.nasa.gov/java/
2. Start Eclipse and to to File -> New -> Project. Choose a Java Project.

3. Copy the content of the src folder in worldwind folder in the src folder of the new
project created in Eclipse.

4. Create a new folder lib inside the Eclipse project folder.

5. Copy all the jar libraries in the worldwind folder inside the folder lib.

6. Import all the libraries inside the Eclipse project: Properties of the project -> Java Build Path -> Libraries -> Add JARs.

Now you should be able to modify and run all the Worldwind demos in the gov.nasa.worldwindx.example folder.



## Importing DAIDALUS Connector in Wodlwind

1. Copy the ./daidalus folder of the DAILUS Connector folder in the src/gov/nasa/worldwindx/examples folder of the Worldwind project in Eclipse.

2. Import the libraries inside lib folder of DAIDALUS Connector in the Eclipse project like before. These libraries are:
	– pvsio-web-connector for communicating with PVSio-web.
	– DAIDALUS_noV-0.9b containing the DAIDALUS java implementation.
	
Now you should be able to execute the DAIDALUS Connector example by simply running the main class Start in the daidalus folder.