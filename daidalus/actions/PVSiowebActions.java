/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.actions;

import gov.nasa.worldwindx.examples.daidalus.rendering.objects.Arc;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import pvsioweb.PVSioweb;


public class PVSiowebActions extends PVSioweb {

    protected boolean usePVS;
    protected boolean pvsIsReady;
    protected boolean parametersLoadedPVS;
    private DaidalusActions da;
    private RenderingActions ra;

    public PVSiowebActions(String uri, DaidalusActions daidalusActions, RenderingActions renderingActions) {
        super(uri);
        this.da = daidalusActions;
        this.ra = renderingActions;
        usePVS = false;
        pvsIsReady = false;
        parametersLoadedPVS = false;
    }

    @Override
    public void onMessage(String message) {
        if (message.length() > 5) {
            String subMessage = message.substring(2, message.length() - 2);
            ArrayList<Arc> arcs = daidalusXMLParser(message.substring(2, message.length() - 2));

            ra.setArchs(arcs);
            da.view.setAircraftText(Tools.prettyPrint(da.daa.getOwnship(), arcs, da.currentTime));

            if (da.eventBase) {
                da.actionsPVS();
            }
        }
    }

    @Override
    public void onProcessStarted(String message) {
        System.out.println(message + "\n<Process Started> ");
        this.setPvsIsReady(true);
    }

    private void setPvsIsReady(boolean pvsIsReady) {
        this.pvsIsReady = pvsIsReady;
        da.view.setPVSReady(true);
        checkLoadingConfigPVS();
        checkUsingPVS();
    }

    public void usePVS() {
        usePVS = true;
        checkLoadingConfigPVS();
        checkUsingPVS();
    }

    public void useJAVA() {
        usePVS = false;
        da.switchToPVS = false;
    }

    private void checkLoadingConfigPVS() {
        if (da.config_file != null && pvsIsReady && !parametersLoadedPVS) {
            da.daa.loadParametersFromFile(da.config_file);
            this.evalExpr("load_parameters(" + da.daa.getParameters().toPVS(da.PRECISION) + ");");
            parametersLoadedPVS = true;
        }
    }

    private void checkUsingPVS() {
        if (pvsIsReady && parametersLoadedPVS && usePVS && da.input_file != null && da.timer.isRunning()) {
            da.switchToPVS = true;
        }
    }

    private ArrayList<Arc> daidalusXMLParser(String message) {
        ArrayList<Arc> arcs = new ArrayList<Arc>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(message));
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("bands");
            for (int j = 0; j < nList.getLength(); j++) {
                if (nList.item(j).getAttributes().getNamedItem("type").getNodeValue().equals("trk")) {
                    Arc arc;
                    Element element = (Element) nList.item(j);
                    NodeList bands = element.getElementsByTagName("band");
                    for (int b = 0; b < bands.getLength(); b++) {
                        Element band = (Element) bands.item(b);
                        String region = band.getAttribute("region");
                        NodeList lower = band.getElementsByTagName("lower");
                        double start = Double.valueOf(lower.item(0).getFirstChild().getNodeValue());
                        NodeList upper = band.getElementsByTagName("upper");
                        double end = Double.valueOf(upper.item(0).getFirstChild().getNodeValue());
                        arc = new Arc(Math.toRadians(start), Math.toRadians(end), region);
                        arcs.add(arc);
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arcs;
    }
}
