/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.actions;

import gov.nasa.larcfm.ACCoRD.Daidalus;
import gov.nasa.larcfm.ACCoRD.KinematicBands;
import gov.nasa.larcfm.ACCoRD.OwnshipState;
import gov.nasa.larcfm.ACCoRD.TrafficState;
import gov.nasa.larcfm.Util.Position;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.Arc;
import gov.nasa.worldwindx.examples.daidalus.tools.DaidalusWalker;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class DaidalusActions implements ActionListener {

    protected Daidalus daa;
    private DaidalusWalker walker;
    protected Timer timer;
    private RenderingActions renderingActions;
    protected View view;

    protected String config_file = null;
    protected String input_file = null;

    private int timerDelay;
    private double startingTime;

    private static double defaultCameraAltitude = 100000.0;

    private String followingAircraft;
    private boolean flyover;

    protected double currentTime = 0;

    private PVSiowebActions pvSiowebActions;
    protected static int PRECISION = 8;

    protected boolean switchToPVS;

    protected boolean eventBase;

    public void setFollowingAircraft(String followingAircraft) {
        if (!followingAircraft.equals(this.followingAircraft)) {
            flyover = true;
        }
        this.followingAircraft = followingAircraft;
    }

    public DaidalusActions() {
        daa = new Daidalus();
        view = new View(this);
        renderingActions = new RenderingActions(view);
        timerDelay = 5000;
        startingTime = 0.0;
        followingAircraft = "Ownship";
        flyover = false;
        switchToPVS = false;
        eventBase = false;
        startPVS();
    }

    public DaidalusActions(boolean startPVS) {
        daa = new Daidalus();
        view = new View(this);
        renderingActions = new RenderingActions(view);
        timerDelay = 5000;
        startingTime = 0.0;
        followingAircraft = "Ownship";
        flyover = false;
        switchToPVS = false;
        eventBase = false;
        if (startPVS) startPVS();
    }

    public DaidalusActions(String input_file, String config_file, int timerSpeed, int timerDelay, boolean startPVS) {
        daa = new Daidalus();
        view = new View(this);
        renderingActions = new RenderingActions(view);
        timerDelay = 5000;
        startingTime = 0.0;
        this.input_file = input_file;
        this.config_file = config_file;
        timer = new Timer(timerSpeed, this);
        timer.setInitialDelay(timerDelay);
        this.flyover = false;
        switchToPVS = false;
        eventBase = false;
        if (startPVS) startPVS();
    }

    public void setInputFile(String filename) {
        if (input_file == null || !input_file.equals(filename)) {
            input_file = filename;
            System.out.println("Scenario File: " + filename);
        } else {
            return;
        }
    }

    public void setConfigFile(String filename) {
        if (config_file == null || !config_file.equals(filename)) {
            config_file = filename;
            System.out.println("Config File: " + config_file);
        } else {
            return;
        }
    }

    public void setTimer(int timerSpeed, int timerDelay) {
        if (timer != null) {
            timer.stop();
            System.out.println("Timer stopped");
        }
        timer = new Timer(timerSpeed, this);
        timer.setInitialDelay(timerDelay);
        System.out.println("New Timer: " + timerSpeed + "speed; " + timerDelay + "delay;");
    }

    public void setTimer(int timerSpeed) {
        if (timer != null) {
            timer.stop();
            System.out.println("Timer stopped");
        }
        timer = new Timer(timerSpeed, this);
        timer.setInitialDelay(timerDelay);
        System.out.println("New Timer: " + timerSpeed + "speed; " + timerDelay + "delay;");
    }

    private void initialize(){
        daa.loadParametersFromFile(config_file);
        System.out.println("Config File Loaded <- " + config_file);
        walker = new DaidalusWalker(input_file);
        System.out.println("Input File Loaded <- " + input_file);
        walker.goToTime(startingTime);
        System.out.println("Starting from time: " + startingTime);
    }

    public void start() {
        if (config_file == null || input_file == null) {
            if (config_file == null) {
                System.err.println("Please select a configuration file first.");
//                view.setDisplayText("Please select a configuration file first.");
            }
            if (input_file == null) {
                System.err.println("Please select an input file first.");
//                view.setDisplayText("Please select an input file first.");
            }
            return;
        }
        initialize();

        // Load WorldWind then position the View
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        goToFirstScene();
                    }
                },
                1000
        );
        timer.start();
    }

    public void stop() {
        if(timer!=null){
            timer.stop();
            System.out.println("Timer stopped");
        }
        else{
        }
    }

    public void setStartingPoint(double timePoint) {
        startingTime = timePoint;
        System.out.println("Starting Point = " + timePoint);
    }

    public void actionsPVS() {
        if (!walker.atEnd()) {
            currentTime = walker.currentTime();
            walker.readState(daa, followingAircraft);
            drawAndFollow(daa);
            renderingActions.addDadalusCompass();
            OwnshipState own = daa.getOwnship();
            pvSiowebActions.evalExpr("daidalus(" + daa.aircraftToPVS(PRECISION) + ",deg,nounit,nounit);");
        }
    }

    private void actionsJAVA(){
        if (!walker.atEnd()) {
            currentTime = walker.currentTime();
            walker.readState(daa, followingAircraft);
            drawAndFollow(daa);
            renderingActions.addDadalusCompass();

            OwnshipState own = daa.getOwnship();

            KinematicBands kb = daa.getKinematicBands();
            ArrayList<Arc> arcs = new ArrayList<Arc>();
            Arc arc;
            for (int i = 0; i < kb.trackLength(); ++i) {
                double start = kb.track(i, "deg").low;
                double end = kb.track(i, "deg").up;
                String proxymity = kb.trackRegion(i).name();
                arc = new Arc(Math.toRadians(start), Math.toRadians(end), proxymity);
                arcs.add(arc);
            }

            renderingActions.setArchs(arcs);
            view.setAircraftText(Tools.prettyPrint(own, arcs, currentTime));

        }
    }


    private void drawAndFollow(Daidalus daa){
        OwnshipState own = daa.getOwnship();
        Position ownPosition = own.getPosition();
        Angle owangle = Angle.fromRadians(Math.PI / 2.0 - own.getVelocity().compassAngle());
        if (!view.aircraftRegistered(own.getId())) {
            renderingActions.addAircraft(own.getId(), ownPosition.latitude(), ownPosition.longitude(), ownPosition.altitude(), owangle);
        } else {
            renderingActions.moveAircraft(own.getId(), ownPosition.latitude(), ownPosition.longitude(), ownPosition.altitude(), owangle);
        }

        for (int i = 1; i < daa.numberOfAircraft(); ++i) {
            TrafficState ac = daa.getTraffic(i);
            Position iPosition = ac.getPosition();
            Angle intangle = Angle.fromRadians(Math.PI / 2.0 - ac.getVelocity().compassAngle());
            if (!view.aircraftRegistered(ac.getId())) {
                renderingActions.addAircraft(ac.getId(), iPosition.latitude(), iPosition.longitude(), iPosition.altitude(), intangle);
            } else {
                renderingActions.moveAircraft(ac.getId(), iPosition.latitude(), iPosition.longitude(), iPosition.altitude(), intangle);
            }
        }

        Angle cameraAngle = Angle.fromRadians(own.getVelocity().compassAngle());
        if (flyover) {
            view.flyToPosition(ownPosition.latitude(), ownPosition.longitude(), defaultCameraAltitude);
            flyover = false;
        } else {
            view.focusPosition(ownPosition.latitude(), ownPosition.longitude(), cameraAngle);
        }
    }


    public void actionPerformed(ActionEvent e) {
        if(!switchToPVS){
            actionsJAVA();
        }
        else{
            actionsPVS();
        }
    }

    private void goToFirstScene() {
        double currentTime = walker.currentTime();
        walker.goNext();
        walker.readState(daa);
        OwnshipState own = daa.getOwnship();
        Position ownPosition = own.getPosition();
        view.flyToPosition(ownPosition.latitude(), ownPosition.longitude(), defaultCameraAltitude);
        walker.goPrev();
    }

    public void startPVS() {
        pvSiowebActions = new PVSiowebActions("localhost:8082/", this, renderingActions);
        if (pvSiowebActions.connect()) {
            view.setPVSReady(false);
            pvSiowebActions.startPVSioDemo("DAIDALUS/pvs", "daidalus_io");
        } else {
            System.err.println("PVSio-web is not running. Please start PVSio-web first by executing ./start.sh from the pvsio-web folder.");
        }
    }

    public void usePVS(){
        pvSiowebActions.usePVS();
    }

    public void useJAVA(){
        pvSiowebActions.useJAVA();
    }

    public boolean switchToEventBased(){
        if(switchToPVS){
            if(timer != null && timer.isRunning()){
                stop();
            }
            eventBase = true;
            return true;
        }
        return false;
    }

    public boolean switchToTimerBased(){
        eventBase = false;
        if(timer != null && !timer.isRunning()){
            start();
        }
        return true;
    }

}



