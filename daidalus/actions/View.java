package gov.nasa.worldwindx.examples.daidalus.actions;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwindx.examples.daidalus.actions.DaidalusActions;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.AircraftPolygon;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.DaidalusCompass;
import gov.nasa.worldwindx.examples.daidalus.rendering.panel.AircraftDisplay;
import gov.nasa.worldwindx.examples.daidalus.rendering.panel.InfoDisplay;
import gov.nasa.worldwindx.examples.daidalus.rendering.panel.ObjectPositioning;
import gov.nasa.worldwindx.examples.daidalus.rendering.panel.ViewController;

import java.util.ArrayList;
import java.util.HashMap;

public class View {

    private ObjectPositioning objectPositioning;
    private ObjectPositioning.AppFrame appFrame;
    private ViewController viewController;
    private AircraftDisplay aircraftDisplay;
    private InfoDisplay infoDisplay;
    private int zoomFactor;
    private DaidalusActions daidalusActions;
    private HashMap <String, AircraftPolygon> id_aircraft;
    protected WorldWindow wwd;

    private ArrayList<String> text;



    public View(DaidalusActions daidalusActions){
        this.daidalusActions = daidalusActions;
        objectPositioning = new ObjectPositioning(daidalusActions);
        appFrame = objectPositioning.start("Daidalus View", objectPositioning);
        this.wwd = appFrame.getWwd();
        assert appFrame != null;
        viewController = appFrame.getViewController();
        aircraftDisplay = appFrame.getAircraftDisplay();
        infoDisplay = appFrame.getInfoDisplay();
        zoomFactor = 35;
        id_aircraft = new HashMap<String, AircraftPolygon>();
    }

    public WorldWindow getWwd() {
        return wwd;
    }

    public DaidalusActions getDaidalusActions(){
        return daidalusActions;
    }


    public void removeAllObjects(){
        appFrame.removeAllObjects();
        id_aircraft.clear();
    }



    public void addAircraft(AircraftPolygon aircraftPolygon){
        appFrame.addAircraft(aircraftPolygon);
        id_aircraft.put(aircraftPolygon.getId(), aircraftPolygon);
    }

    public void moveAircraft(String id, double latitude, double longitude, double altitude, Angle heading){
        AircraftPolygon aircraftPolygon = id_aircraft.get(id);
        aircraftPolygon.goTo(latitude, longitude, altitude, heading);
    }

    public void focusPosition(double latitude, double longitude, Angle angle){
        LatLon latLon = new LatLon(Angle.fromDegrees(latitude), Angle.fromDegrees(longitude));
        viewController.setView(latLon, angle);
    }

    public void flyToPosition(double latitude, double longitude){
        LatLon latLon = new LatLon(Angle.fromDegrees(latitude), Angle.fromDegrees(longitude));
        viewController.flyToPosition(latLon);
    }

    public void flyToPosition(double latitude, double longitude, double altitude){
        LatLon latLon = new LatLon(Angle.fromDegrees(latitude), Angle.fromDegrees(longitude));
        viewController.flyToPosition(latLon, altitude);
    }

    public boolean aircraftRegistered(String id){
        if(id_aircraft.containsKey(id)){
            return true;
        }
        else{
            return false;
        }
    }

    public void addLayer(RenderableLayer renderableLayer){
        appFrame.addLayer(renderableLayer);
    }

    public void setAircraftText(ArrayList<String> infos){
        text = infos;
        aircraftDisplay.setDisplayedText(infos);
    }

    public void addAircraftText(ArrayList<String> infos){
        if(text != null){
            for(String text1 : infos){
                this.text.add(text1);
            }
        }
        aircraftDisplay.setDisplayedText(text);
    }

    public void setDisplayText(String info){
        infoDisplay.setDisplayedText(info);
    }

    public void setPVSReady(boolean ready){
        appFrame.setPVSReady(ready);
    }

}



