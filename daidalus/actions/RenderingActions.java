/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.actions;

import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.AircraftPolygon;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.Arc;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.DaidalusCompass;

import java.util.ArrayList;

public class RenderingActions {

    private gov.nasa.worldwindx.examples.daidalus.actions.View view;
    protected DaidalusCompass daidalusCompass;

    public RenderingActions(gov.nasa.worldwindx.examples.daidalus.actions.View view) {
        this.view = view;
    }

    public void addDadalusCompass() {
        if (daidalusCompass != null) {
            daidalusCompass.removeAllRenderables();
        }
            daidalusCompass = new DaidalusCompass();
            view.addLayer(daidalusCompass);
            daidalusCompass.setLocationOffset(new Vec4(0, 50));
    }

    public void addAircraft(String id, double latitude, double longitude, double altitude, Angle heading) {
        AircraftPolygon aircraftPolygon = new AircraftPolygon(id, latitude, longitude, altitude, heading, view);
        view.addAircraft(aircraftPolygon);
    }

    public void moveAircraft(String id, double latitude, double longitude, double altitude, Angle heading) {
        view.moveAircraft(id, latitude, longitude, altitude, heading);
    }

    public void setArchs(ArrayList<Arc> arcs) {
        daidalusCompass.setArcs(arcs);
    }
}



