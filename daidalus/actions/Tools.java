/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.actions;

import gov.nasa.larcfm.ACCoRD.OwnshipState;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.Arc;

import java.util.ArrayList;

public class Tools {

    public static ArrayList<String> prettyPrint(OwnshipState own, double currentTime) {
        ArrayList<String> infos = new ArrayList<String>();
        infos.add("TIME:    " + currentTime + "\n");
        infos.add("ID:    " + own.getId());
        infos.add("POS:    " + Math.round(own.getPosition().latitude() * 100.0) / 100.0 + ", " + Math.round(own.getPosition().longitude() * 100.0) / 100.0);
        double gs = own.getVelocity().gs();
        double ts = own.getVelocity().trk();
        double vs = own.getVelocity().vs();
        infos.add("VEL:   " + Math.round(gs * 100.0) / 100.0 + ", " + Math.round(ts * 100.0) / 100.0 + ", " + Math.round(vs * 100.0) / 100.0);
        infos.add("ANGLE: " + Math.round(Angle.fromRadians(own.getVelocity().compassAngle()).degrees * 100.0) / 100.0 + "\n\n");
        return infos;
    }

    public static  ArrayList<String> prettyPrint(ArrayList<Arc> arcs) {
        ArrayList<String> infos = new ArrayList<String>();
        String arcs_temp;
        boolean arcs_show = false;
        arcs_temp = ("OK: ");
        for (Arc arc : arcs) {
            if (arc.getNear().equals("NONE")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        arcs_temp = ("NEAR: ");
        arcs_show = false;
        for (Arc arc : arcs) {
            if (arc.getNear().equals("NEAR")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        arcs_temp = ("RECOVERY: ");
        arcs_show = false;
        for (Arc arc : arcs) {
            if (arc.getNear().equals("RECOVERY")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        return infos;
    }


    public static  ArrayList<String> prettyPrint(OwnshipState own, ArrayList<Arc> arcs, double currentTime) {
        ArrayList<String> infos = new ArrayList<String>();

        infos.add("TIME:    " + currentTime + "\n");
        infos.add("ID:    " + own.getId());
        infos.add("POS:    " + Math.round(own.getPosition().latitude() * 100.0) / 100.0 + ", " + Math.round(own.getPosition().longitude() * 100.0) / 100.0);
        double gs = own.getVelocity().gs();
        double ts = own.getVelocity().trk();
        double vs = own.getVelocity().vs();
        infos.add("VEL:   " + Math.round(gs * 100.0) / 100.0 + ", " + Math.round(ts * 100.0) / 100.0 + ", " + Math.round(vs * 100.0) / 100.0);
        infos.add("ANGLE: " + Math.round(Angle.fromRadians(own.getVelocity().compassAngle()).degrees * 100.0) / 100.0 + "\n\n");

        String arcs_temp;
        boolean arcs_show = false;
        arcs_temp = ("OK: ");
        for (Arc arc : arcs) {
            if (arc.getNear().equals("NONE")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        arcs_temp = ("NEAR: ");
        arcs_show = false;
        for (Arc arc : arcs) {
            if (arc.getNear().equals("NEAR")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        arcs_temp = ("RECOVERY: ");
        arcs_show = false;
        for (Arc arc : arcs) {
            if (arc.getNear().equals("RECOVERY")) {
                arcs_temp = arcs_temp.concat("\n" + Math.round(Math.toDegrees(arc.getStart()) * 100.0) / 100.0 + "-" + Math.round(Math.toDegrees(arc.getEnd()) * 100.0) / 100.0);
                arcs_show = true;
            }
        }
        arcs_temp = arcs_temp.concat("\n");
        if (arcs_show) infos.add(arcs_temp);
        return infos;
    }

}
