/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.panel;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwind.view.ViewUtil;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

public class ViewController extends BasicOrbitView {

    protected WorldWindow wwd;
    protected ViewUtil.ViewState viewState;

    public ViewController(WorldWindow wwd) {
        super();
        this.wwd = wwd;
        this.wwd.setView(this);
    }


    public void setView(LatLon latLon, Angle angle) {
        Position position = new Position(latLon, 0);
        this.setCenterPosition(position);
        this.setHeading(angle);
    }

    public void flyToPosition(LatLon latLon) {
        double altitude = this.wwd.getCurrentPosition().getAltitude();
        Position position = new Position(latLon, altitude);
        this.wwd.getView().goTo(position, altitude);
    }

    public void flyToPosition(LatLon latLon, double altitude) {
        Position position = new Position(latLon, altitude);
        this.wwd.getView().goTo(position, altitude);
    }


}
