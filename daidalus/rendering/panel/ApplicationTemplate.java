/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.panel;

import com.jogamp.newt.event.KeyEvent;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.event.RenderingExceptionListener;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.exception.WWAbsentRequirementException;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.ViewControlsLayer;
import gov.nasa.worldwind.layers.ViewControlsSelectListener;
import gov.nasa.worldwind.layers.WorldMapLayer;
import gov.nasa.worldwind.util.StatisticsPanel;
import gov.nasa.worldwind.util.StatusBar;
import gov.nasa.worldwind.util.WWUtil;
import gov.nasa.worldwindx.examples.ClickAndGoSelectListener;
import gov.nasa.worldwindx.examples.LayerPanel;
import gov.nasa.worldwindx.examples.daidalus.actions.DaidalusActions;
import gov.nasa.worldwindx.examples.daidalus.tools.PanelTools;
import gov.nasa.worldwindx.examples.util.HighlightController;
import gov.nasa.worldwindx.examples.util.ToolTipController;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ApplicationTemplate {

    public static JMenuBar menubar = new JMenuBar();
    private DaidalusActions daidalusActions;

    public ApplicationTemplate(DaidalusActions daidalusActions) {
        this.daidalusActions = daidalusActions;
    }

    public gov.nasa.worldwindx.examples.daidalus.rendering.panel.ObjectPositioning.AppFrame start(String appName, gov.nasa.worldwindx.examples.daidalus.rendering.panel.ObjectPositioning objectPositioning) {
        if (Configuration.isMacOS() && appName != null) {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", appName);
        }

        try {
            gov.nasa.worldwindx.examples.daidalus.rendering.panel.ObjectPositioning.AppFrame frame = objectPositioning.getNewAppFrame();
            frame.setTitle(appName);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    frame.setVisible(true);
                }
            });
            return frame;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class AppPanel extends JPanel {
        protected WorldWindow wwd;
        protected StatusBar statusBar;
        protected ToolTipController toolTipController;
        protected HighlightController highlightController;

        public AppPanel(Dimension canvasSize, boolean includeStatusBar) {
            super(new BorderLayout());

            this.wwd = this.createWorldWindow();
            ((Component) this.wwd).setPreferredSize(canvasSize);

            // Create the default model as described in the current worldwind
            // properties.
            Model m = (Model) WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);
            this.wwd.setModel(m);

            // Setup a select listener for the worldmap click-and-go feature
            this.wwd.addSelectListener(new ClickAndGoSelectListener(this.getWwd(), WorldMapLayer.class));

            this.add((Component) this.wwd, BorderLayout.CENTER);
            if (includeStatusBar) // status bar at the bottom
            {
                this.statusBar = new StatusBar();
                this.add(statusBar, BorderLayout.PAGE_END);
                this.statusBar.setEventSource(wwd);
            }

            // Add controllers to manage highlighting and tool tips.
            this.toolTipController = new ToolTipController(this.getWwd(), AVKey.DISPLAY_NAME, null);
            this.highlightController = new HighlightController(this.getWwd(), SelectEvent.ROLLOVER);
        }

        protected WorldWindow createWorldWindow() {
            return new WorldWindowGLCanvas();
        }

        public WorldWindow getWwd() {
            return wwd;
        }

        public StatusBar getStatusBar() {
            return statusBar;
        }
    }

    public class AppFrame extends JFrame {
        protected AppPanel wwjPanel;
        protected LayerPanel layerPanel;
        protected StatisticsPanel statsPanel;
        private Dimension canvasSize = new Dimension(1200, 900);

        private JRadioButtonMenuItem pvsRadio;
        private boolean pvsready;

        public AppFrame() {
            this.initialize(true, true, false);
        }

        public void setPVSReady(boolean ready){
            pvsready = ready;
            if(!ready){
                pvsRadio.setText("PVS [loading...]");
            }
            else{
                pvsRadio.setText("PVS");
            }
        }


        public AppFrame(Dimension size) {
            this.canvasSize = size;
            this.initialize(true, true, false);
        }

        public AppFrame(boolean includeStatusBar, boolean includeLayerPanel, boolean includeStatsPanel) {
            this.initialize(includeStatusBar, includeLayerPanel, includeStatsPanel);
        }

        protected void initialize(boolean includeStatusBar, boolean includeLayerPanel, boolean includeStatsPanel) {
            JMenuBar jm = createMenuBar();

            this.wwjPanel = this.createAppPanel(this.canvasSize, includeStatusBar);
            this.wwjPanel.setPreferredSize(canvasSize);

            this.getContentPane().add(wwjPanel, BorderLayout.CENTER);
            this.getContentPane().add(jm, BorderLayout.NORTH);

            if (includeLayerPanel) {
                this.layerPanel = new LayerPanel(this.wwjPanel.getWwd(), null);
            }

            if (includeStatsPanel || System.getProperty("gov.nasa.worldwind.showStatistics") != null) {
                this.statsPanel = new StatisticsPanel(this.wwjPanel.getWwd(), new Dimension(250, canvasSize.height));
                this.getContentPane().add(this.statsPanel, BorderLayout.EAST);
            }

            // Create and install the view controls layer and register a
            // controller for it with the World Window.
            ViewControlsLayer viewControlsLayer = new ViewControlsLayer();
            PanelTools.insertBeforeCompass(getWwd(), viewControlsLayer);
            this.getWwd().addSelectListener(new ViewControlsSelectListener(this.getWwd(), viewControlsLayer));

            // Register a rendering exception listener that's notified when
            // exceptions occur during rendering.
            this.wwjPanel.getWwd().addRenderingExceptionListener(
                    new RenderingExceptionListener() {
                        public void exceptionThrown(Throwable t) {
                            if (t instanceof WWAbsentRequirementException) {
                                String message = "Computer does not meet minimum graphics requirements.\n";
                                message += "Please install up-to-date graphics driver and try again.\n";
                                message += "Reason: " + t.getMessage()
                                        + "\n";
                                message += "This program will end when you press OK.";

                                JOptionPane.showMessageDialog(
                                        AppFrame.this, message,
                                        "Unable to Start Program",
                                        JOptionPane.ERROR_MESSAGE);
                                System.exit(-1);
                            }
                        }
                    });

            // Search the layer list for layers that are also select
            // listeners and register them with the World
            // Window. This enables interactive layers to be included without
            // specific knowledge of them here.
            for (Layer layer : this.wwjPanel.getWwd().getModel().getLayers()) {
                if (layer instanceof SelectListener) {
                    this.getWwd().addSelectListener((SelectListener) layer);
                }
            }

            this.pack();

            // Center the application on the screen.
            WWUtil.alignComponent(null, this, AVKey.CENTER);
            this.setResizable(true);
        }

        private JMenuBar createMenuBar() {

            menubar.setBackground(Color.gray);
            menubar.setLayout(new FlowLayout(FlowLayout.LEFT));
            final String[] input_file = new String[2];

            JMenu jMenu = new JMenu("Options");
            jMenu.setMnemonic(KeyEvent.VK_F);
            jMenu.setOpaque(false);
            JMenuItem openfile = new JMenuItem("Open Scenario File...");
            openfile.setMnemonic(KeyEvent.VK_O);
            openfile.setToolTipText("Open the scenario file");
            openfile.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    JFileChooser saveFile = new JFileChooser();
                    saveFile.showOpenDialog(null);
                    input_file[0] = saveFile.getSelectedFile().getPath();
                    System.out.println("The scenario file is: " + input_file[0]);
                    daidalusActions.setInputFile(input_file[0]);
                }
            });

            JMenuItem configfile = new JMenuItem("Open Configuration File...");
            configfile.setMnemonic(KeyEvent.VK_O);
            configfile.setToolTipText("Open the configuration file");
            configfile.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    JFileChooser saveFile = new JFileChooser();
                    saveFile.showOpenDialog(null);
                    input_file[1] = saveFile.getSelectedFile().getPath();
                    System.out.println("The config file is: " + input_file[1]);
                    daidalusActions.setConfigFile(input_file[1]);
                }
            });

            JMenuItem exit = new JMenuItem("Exit");
            exit.setMnemonic(KeyEvent.VK_E);
            exit.setToolTipText("Exit application");
            exit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    System.exit(0);
                }
            });

            JMenuItem quickLoad = new JMenuItem("Quick Load");
            quickLoad.setMnemonic(KeyEvent.VK_ENTER);
            quickLoad.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    daidalusActions.setInputFile("./src/gov/nasa/worldwindx/examples/daidalus/data/test.txt");
                    daidalusActions.setConfigFile("./src/gov/nasa/worldwindx/examples/daidalus/data/default_parameters.txt");
                }
            });

            jMenu.add(configfile);
            jMenu.add(openfile);
            jMenu.add(quickLoad);
            jMenu.add(exit);
            menubar.add(jMenu);

            JLabel jLabel_start = new JLabel("Start from:");
            menubar.add(jLabel_start);

            JTextField jTextField_start = new JTextField("0", 3);
            //jTextField_start.setPreferredSize(new Dimension(20, 20));
            jTextField_start.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                }
            });
            menubar.add(jTextField_start);

            JLabel jLabel_speed = new JLabel("Sampling speed (ms):");
            menubar.add(jLabel_speed);
            JTextField jTextField_speed = new JTextField("800", 5);
            //jTextField_speed.setPreferredSize(new Dimension(20, 20));
            jTextField_speed.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                }
            });
            menubar.add(jTextField_speed);

            JButton jButton = new JButton("Run");
            jButton.setMnemonic(KeyEvent.VK_ENTER);
            jButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    if (input_file[0] != null) {
                        daidalusActions.setInputFile(input_file[0]);
                    }
                    if (input_file[1] != null) {
                        daidalusActions.setConfigFile(input_file[1]);
                    }
                    daidalusActions.setTimer(Integer.parseInt(jTextField_speed.getText()));
                    daidalusActions.setStartingPoint(Double.parseDouble(jTextField_start.getText()));
                    daidalusActions.start();
                }
            });
            menubar.add(jButton);

            jButton = new JButton("Stop");
            jButton.setMnemonic(KeyEvent.VK_ENTER);
            jButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    daidalusActions.stop();
                }
            });
            menubar.add(jButton);

            ButtonGroup engine = new ButtonGroup();
            pvsRadio = new JRadioButtonMenuItem("PVS");
            pvsRadio.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    daidalusActions.usePVS();
                }
            });
            engine.add(pvsRadio);
            menubar.add(pvsRadio);
            JRadioButtonMenuItem menuItem;
            menuItem = new JRadioButtonMenuItem("JAVA");
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    daidalusActions.useJAVA();
                }
            });
            menuItem.setSelected(true);
            engine.add(menuItem);
            menubar.add(menuItem);

            JCheckBox checkBox;
            checkBox = new JCheckBox("PVS-Event-Based");
            checkBox.setToolTipText("Instead of being timer-based, the action will be continuously triggered by the speed of the PVS response");
            checkBox.setSelected(false);
            checkBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event) {
                    if (checkBox.isSelected()) {
                        boolean res = daidalusActions.switchToEventBased();
                        checkBox.setSelected(res);
                    } else {
                        boolean res = daidalusActions.switchToTimerBased();
                        checkBox.setSelected(!res);
                    }
                }
            });
            menubar.add(checkBox);


            return menubar;
        }

        protected AppPanel createAppPanel(Dimension canvasSize, boolean includeStatusBar) {
            return new AppPanel(canvasSize, includeStatusBar);
        }

        public Dimension getCanvasSize() {
            return canvasSize;
        }

        public AppPanel getWwjPanel() {
            return wwjPanel;
        }

        public WorldWindow getWwd() {
            return wwjPanel.getWwd();
        }

        public StatusBar getStatusBar() {
            return this.wwjPanel.getStatusBar();
        }

        public LayerPanel getLayerPanel() {
            return layerPanel;
        }

        public StatisticsPanel getStatsPanel() {
            return statsPanel;
        }

        public void setToolTipController(ToolTipController controller) {
            if (this.wwjPanel.toolTipController != null)
                this.wwjPanel.toolTipController.dispose();

            this.wwjPanel.toolTipController = controller;
        }

        public void setHighlightController(HighlightController controller) {
            if (this.wwjPanel.highlightController != null)
                this.wwjPanel.highlightController.dispose();
            this.wwjPanel.highlightController = controller;
        }
    }
}
