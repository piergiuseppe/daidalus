/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.panel;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.ScreenAnnotation;
import gov.nasa.worldwind.util.Logging;
import java.awt.*;
import java.util.ArrayList;

public class AircraftDisplay extends RenderableLayer
{
    protected WorldWindow wwd;
    protected boolean update = true;

    private ScreenAnnotation annotation;
    protected Dimension size;
    private Color color = Color.decode("#b0b0b0");
    private Color highlightColor = Color.decode("#ffffff");
    private double minOpacity = .6;
    private double maxOpacity = 1;
    private char layerEnabledSymbol = '\u25a0';
    private Font font = new Font("CourierNew", Font.PLAIN, 14);
    private boolean minimized = false;
    private int borderWidth = 20; // TODO: make configurable
    private String position = AVKey.NORTHWEST; // TODO: make configurable
    private Vec4 locationCenter = null;
    private Vec4 locationOffset = null;
    private ArrayList<String> displayedText;
    private int yMargin = 160;

    // Dragging
    protected int dragRefIndex = -1;
    protected Color dragColor = Color.RED;

    public AircraftDisplay(WorldWindow wwd)
    {
        if (wwd == null)
        {
            String msg = Logging.getMessage("nullValue.WorldWindow");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.wwd = wwd;
        this.initialize();
    }

    protected void initialize()
    {
        // Set up screen annotation that will display the layer list
        this.annotation = new ScreenAnnotation("", new Point(0, 0));

        // Set annotation so that it will not force text to wrap (large width) and will adjust it's width to
        // that of the text. A height of zero will have the annotation height follow that of the text too.
        this.annotation.getAttributes().setSize(new Dimension(Integer.MAX_VALUE, 0));
        this.annotation.getAttributes().setAdjustWidthToText(AVKey.SIZE_FIT_TEXT);

        // Set appearance attributes
        this.annotation.getAttributes().setCornerRadius(0);
        this.annotation.getAttributes().setFont(this.font);
        this.annotation.getAttributes().setHighlightScale(1);
        this.annotation.getAttributes().setTextColor(Color.WHITE);
        this.annotation.getAttributes().setBackgroundColor(new Color(0f, 0f, 0f, .5f));
        this.annotation.getAttributes().setInsets(new Insets(6, 6, 6, 6));
        this.annotation.getAttributes().setBorderWidth(1);
        this.annotation.getAttributes().setVisible(false);
        this.addRenderable(this.annotation);

    }

    public void render(DrawContext dc)
    {
        if (this.update)
            this.updateNow(dc);

        this.annotation.setScreenPoint(computeLocation(dc.getView().getViewport()));
        super.render(dc);
    }

    public void updateNow(DrawContext dc)
    {
        // Adjust annotation appearance to highlighted state
//        this.highlight(this.annotation.getAttributes().isHighlighted());

        // Compose html text
        if(displayedText != null) {
            String text = this.displayText();
            this.annotation.setText(text);
        }
        // Update current size and adjust annotation draw offset according to it's width
        // TODO: handle annotation scaling
        this.size = this.annotation.getPreferredSize(dc);
        this.annotation.getAttributes().setDrawOffset(new Point(this.size.width / 2, 0));
        // Clear update flag
        this.update = false;
    }


    public void setDisplayedText(ArrayList<String> displayedText) {
        this.displayedText = displayedText;
        this.update = true;
        this.annotation.getAttributes().setVisible(true);
    }

    public String displayText()
    {// Compose html text
        StringBuilder text = new StringBuilder();
        Color color = this.highlightColor;
        int i = 0;
        for (String info : displayedText) {
            text.append("<font color=\"");
            text.append(encodeHTMLColor(color));
            text.append("\">");
            text.append(layerEnabledSymbol);
            text.append(" ");
            text.append("<i>");
            text.append(info.replaceAll("(\r\n|\n)", "<br />"));
            text.append("</i>");
            text.append("<br />");
            i++;
        }
        return text.toString();
    }


    protected static String encodeHTMLColor(Color c)
    {
        return String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
    }


    /**
     * Get the <code>ScreenAnnotation</code> used to display the layer list.
     *
     * @return the <code>ScreenAnnotation</code> used to display the layer list.
     */
    public ScreenAnnotation getAnnotation()
    {
        return this.annotation;
    }


    public Font getFont()
    {
        return this.font;
    }

    /**
     * Set the <code>Font</code> used to draw the layer list text.
     *
     * @param font the <code>Font</code> used to draw the layer list text.
     */
    public void setFont(Font font)
    {
        if (font == null)
        {
            String message = Logging.getMessage("nullValue.FontIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        if (!this.font.equals(font))
        {
            this.font = font;
            this.annotation.getAttributes().setFont(font);
            this.update();
        }
    }

    /**
     * Get the <code>Color</code> used to draw the layer names and the frame border when they are not highlighted.
     *
     * @return the <code>Color</code> used to draw the layer names and the frame border when they are not highlighted.
     */
    public Color getColor()
    {
        return this.color;
    }

    /**
     * Set the <code>Color</code> used to draw the layer names and the frame border when they are not highlighted.
     *
     * @param color the <code>Color</code> used to draw the layer names and the frame border when they are not
     *              highlighted.
     */
    public void setColor(Color color)
    {
        if (color == null)
        {
            String msg = Logging.getMessage("nullValue.ColorIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.color = color;
        this.update();
    }

    /**
     * Get the <code>Color</code> used to draw the layer names and the frame border when they are highlighted.
     *
     * @return the <code>Color</code> used to draw the layer names and the frame border when they are highlighted.
     */
    public Color getHighlightColor()
    {
        return this.highlightColor;
    }

    /**
     * Set the <code>Color</code> used to draw the layer names and the frame border when they are highlighted.
     *
     * @param color the <code>Color</code> used to draw the layer names and the frame border when they are highlighted.
     */
    public void setHighlightColor(Color color)
    {
        if (color == null)
        {
            String msg = Logging.getMessage("nullValue.ColorIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.highlightColor = color;
        this.update();
    }

    /**
     * Get the opacity applied to the layer list when the cursor is outside it's frame.
     *
     * @return the opacity applied to the layer list when the cursor is outside it's frame.
     */
    public double getMinOpacity()
    {
        return this.minOpacity;
    }

    /**
     * Set the opacity applied to the layer list when the cursor is outside it's frame - zero to one, one is fully
     * opaque.
     *
     * @param opacity the opacity applied to the layer list when the cursor is outside it's frame.
     */
    public void setMinOpacity(double opacity)
    {
        this.minOpacity = opacity;
        this.update();
    }

    /**
     * Get the opacity applied to the layer list when the cursor is inside it's frame.
     *
     * @return the opacity applied to the layer list when the cursor is inside it's frame.
     */
    public double getMaxOpacity()
    {
        return this.maxOpacity;
    }

    /**
     * Set the opacity applied to the layer list when the cursor is inside it's frame - zero to one, one is fully
     * opaque.
     *
     * @param opacity the opacity applied to the layer list when the cursor is inside it's frame.
     */
    public void setMaxOpacity(double opacity)
    {
        this.maxOpacity = opacity;
        this.update();
    }



    /**
     * Get the layer manager frame offset from the viewport borders.
     *
     * @return the number of pixels to offset the layer manager frame from the borders indicated by {@link
     *         #setPosition(String)}.
     */
    public int getBorderWidth()
    {
        return borderWidth;
    }

    /**
     * Sets the layer manager frame offset from the viewport borders.
     *
     * @param borderWidth the number of pixels to offset the layer manager frame from the borders indicated by {@link
     *                    #setPosition(String)}.
     */
    public void setBorderWidth(int borderWidth)
    {
        this.borderWidth = borderWidth;
        this.update();
    }

    /**
     * Returns the current relative layer manager frame position.
     *
     * @return the current layer manager frame position
     */
    public String getPosition()
    {
        return position;
    }

    /**
     * Sets the relative viewport location to display the layer manager. Can be one of {@link AVKey#NORTHEAST} (the
     * default), {@link AVKey#NORTHWEST}, {@link AVKey#SOUTHEAST}, or {@link AVKey#SOUTHWEST}. These indicate the corner
     * of the viewport to place the frame.
     *
     * @param position the desired layer manager position
     */
    public void setPosition(String position)
    {
        if (position == null)
        {
            String message = Logging.getMessage("nullValue.ScreenPositionIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }
        this.position = position;
        this.update();
    }


    /** Schedule the layer list for redrawing before the next render pass. */
    public void update()
    {
        this.update = true;
        this.wwd.redraw();
    }



    protected Point computeLocation(Rectangle viewport)
    {
        // TODO: handle annotation scaling
        int width = this.size.width;
        int height = this.size.height;

        int x;
        int y;

        if (this.locationCenter != null)
        {
            x = (int) this.locationCenter.x - width / 2;
            y = (int) this.locationCenter.y - height / 2;
        }
        else if (this.position.equals(AVKey.NORTHEAST))
        {
            x = (int) viewport.getWidth() - width - this.borderWidth;
            y = (int) viewport.getHeight() - height - this.borderWidth;
        }
        else if (this.position.equals(AVKey.SOUTHEAST))
        {
            x = (int) viewport.getWidth() - width - this.borderWidth;
            //noinspection SuspiciousNameCombination
            y = this.borderWidth;
        }
        else if (this.position.equals(AVKey.NORTHWEST))
        {
            x = this.borderWidth;
            y = (int) viewport.getHeight() - height - this.borderWidth - this.yMargin;
        }
        else if (this.position.equals(AVKey.SOUTHWEST))
        {
            x = this.borderWidth;
            //noinspection SuspiciousNameCombination
            y = this.borderWidth;
        }
        else // use North East as default
        {
            x = (int) viewport.getWidth() - width - this.borderWidth;
            y = (int) viewport.getHeight() - height - this.borderWidth;
        }

        if (this.locationOffset != null)
        {
            x += this.locationOffset.x;
            y += this.locationOffset.y;
        }

        return new Point(x, y);
    }

}
