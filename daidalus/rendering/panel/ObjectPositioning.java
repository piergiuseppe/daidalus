/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.panel;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwindx.examples.daidalus.actions.DaidalusActions;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.AircraftPolygon;
import gov.nasa.worldwindx.examples.daidalus.rendering.objects.DaidalusCompass;
import gov.nasa.worldwindx.examples.daidalus.tools.PanelTools;
import gov.nasa.worldwindx.examples.util.ExtentVisibilitySupport;

import javax.media.opengl.GL2;
import java.awt.*;
import java.util.ArrayList;

/**
 * ObjectPositioning demonstrates keeping a set of scene elements visible by using the utility class {@link
 * ExtentVisibilitySupport}. To run this demonstration, execute this class' main
 * method, then follow the on-screen instructions.
 * <p>
 * The key functionality demonstrated by KeepingObjectsVisible is found in the internal classes {@link
 * gov.nasa.worldwindx.examples.KeepingObjectsInView.ViewController} and {@link gov.nasa.worldwindx.examples.KeepingObjectsInView.ViewAnimator}.
 *
 * @author dcollins
 * @version $Id: ObjectPositioning.java 1171 2013-02-11 21:45:02Z dcollins $
 */
public class ObjectPositioning extends ApplicationTemplate {

    public ObjectPositioning(DaidalusActions daidalusActions) {
        super(daidalusActions);
    }

    public AppFrame getNewAppFrame() {
        return new AppFrame();
    }

    public class AppFrame extends ApplicationTemplate.AppFrame {

        protected ViewController viewController;
        protected RenderableLayer shapesLayer;
        private AnnotationLayer annotationLayer;
        protected IconLayer iconLayer;
        protected gov.nasa.worldwindx.examples.daidalus.rendering.panel.AircraftDisplay aircraftDisplay;
        protected InfoDisplay infoDisplay;
        protected GL2 gl2;
        private Balloon monitor;
        private int monitorSizeX = 300;
        private int monitorSizeY = 25;
        private int monitorLettersX = 45;
        private ArrayList<MessageTimeout> messageQueue;
        private DaidalusCompass daidalusCompass;


        private class MessageTimeout {
            private String message;
            private int time;

            public MessageTimeout(String message, int time) {
                this.message = message;
                this.time = time;
            }

            public String getMessage() {
                return message;
            }

            public int getTime() {
                return time;
            }
        }

        public gov.nasa.worldwindx.examples.daidalus.rendering.panel.AircraftDisplay getAircraftDisplay() {
            return aircraftDisplay;
        }

        public InfoDisplay getInfoDisplay() {
            return infoDisplay;
        }

        public AppFrame() {
            this.viewController = new ViewController(this.getWwd());
            iconLayer = new IconLayer();
            shapesLayer = new RenderableLayer();
            aircraftDisplay = new gov.nasa.worldwindx.examples.daidalus.rendering.panel.AircraftDisplay(this.getWwd());
            infoDisplay = new InfoDisplay(this.getWwd());
            getWwd().getModel().getLayers().add(aircraftDisplay);
            getWwd().getModel().getLayers().add(infoDisplay);

//            daidalusCompass = new DaidalusCompass();
//            getWwd().getModel().getLayers().add(daidalusCompass);
//            daidalusCompass.setLocationOffset(new Vec4(0, 50));

            iconLayer.setViewClippingEnabled(false);
            iconLayer.setName("Icons To Track");
            PanelTools.insertBeforePlacenames(this.getWwd(), iconLayer);

            shapesLayer.setName("Shapes to Track");
            PanelTools.insertBeforePlacenames(this.getWwd(), shapesLayer);
            messageQueue = new ArrayList<MessageTimeout>();

        }

        public void addLayer(RenderableLayer renderableLayer) {
            getWwd().getModel().getLayers().add(renderableLayer);
        }

        public ViewController getViewController() {
            return this.viewController;
        }

        public void addObjectsToWorldWindow(Iterable<?> objectsToRender) {
            this.getLayerPanel().update(this.getWwd());

            // Add the objects to track to the layers.
            for (Object o : objectsToRender) {
                if (o instanceof WWIcon)
                    iconLayer.addIcon((WWIcon) o);
                else if (o instanceof Renderable)
                    shapesLayer.addRenderable((Renderable) o);
            }
        }

        public void addAircraft(AircraftPolygon aircraftPolygon) {

            this.getLayerPanel().update(this.getWwd());
            if (aircraftPolygon instanceof WWIcon)
                iconLayer.addIcon((WWIcon) aircraftPolygon);
            else if (aircraftPolygon instanceof Renderable)
                shapesLayer.addRenderable((Renderable) aircraftPolygon);
        }

        public void addIcon(WWIcon icon) {

            this.getLayerPanel().update(this.getWwd());
            iconLayer.addIcon((WWIcon) icon);
        }

        public void removeAllObjects() {
            iconLayer.removeAllIcons();
            shapesLayer.removeAllRenderables();
        }

        private void createAnnotation() {
            AnnotationAttributes defaultAttributes = new AnnotationAttributes();
            defaultAttributes.setBackgroundColor(new Color(0f, 0f, 0f, 0f));
            defaultAttributes.setTextColor(Color.YELLOW);
            defaultAttributes.setLeaderGapWidth(14);
            defaultAttributes.setCornerRadius(0);
            defaultAttributes.setSize(new Dimension(300, 0));
            defaultAttributes.setAdjustWidthToText(AVKey.SIZE_FIT_TEXT); // use strict dimension width - 200
            defaultAttributes.setFont(Font.decode("Arial-BOLD-24"));
            defaultAttributes.setBorderWidth(0);
            defaultAttributes.setHighlightScale(1);             // No highlighting either
            defaultAttributes.setCornerRadius(0);

            ScreenRelativeAnnotation lowerRight = new ScreenRelativeAnnotation("Lower Right", 0.99, 0.01);
            lowerRight.setKeepFullyVisible(true);
            lowerRight.setXMargin(5);
            lowerRight.setYMargin(5);
            lowerRight.getAttributes().setDefaults(defaultAttributes);

            this.annotationLayer.addAnnotation(lowerRight);
        }

        private void createBalloon() {
            monitor = new ScreenAnnotationBalloon("", new Point(10, 200));
            BalloonAttributes attrs = new BasicBalloonAttributes();
            attrs.setSize(Size.fromPixels(300, 50));
            attrs.setOffset(new Offset(0d, 0d, AVKey.PIXELS, AVKey.PIXELS));
            attrs.setInsets(new Insets(10, 10, 10, 10)); // .
            attrs.setLeaderShape(AVKey.SHAPE_NONE);
            attrs.setTextColor(Color.WHITE);
            attrs.setInteriorMaterial(Material.BLACK);
            attrs.setInteriorOpacity(0);
            attrs.setOutlineMaterial(Material.WHITE);
            attrs.setDrawOutline(false);
            monitor.setAttributes(attrs);
            this.shapesLayer.addRenderable(monitor);
            monitor.setVisible(true);
//            monitor.setVisible(false);
        }

        public void displayMessageMonitor(String message) {
            monitor.setText(message);
            monitor.getAttributes().setFont(Font.decode("Courier-PLAIN-15"));
        }

        public void displayMessageMonitor(String message, int ms) {

            if (monitor.isVisible()) {
                // There is already a message showing => queue the message
                messageQueue.add(new MessageTimeout(message, ms));
                System.out.println("MessageQueue ++");
            } else {
                // Calculating the size
                int numberLetters = message.length();
                int numberofLines = 1;
                if (numberLetters > monitorLettersX) {
                    double d = (double) numberLetters / (double) monitorLettersX;
                    numberofLines = (int) Math.ceil(d);
                }
                monitor.getAttributes().setSize(Size.fromPixels(monitorSizeX, monitorSizeY * numberofLines));
                monitor.setText(message);
                monitor.getAttributes().setFont(Font.decode("Courier-PLAIN-12"));
                monitor.setVisible(true);
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                monitor.setVisible(false);
                                if (!messageQueue.isEmpty()) {
                                    // Unlock message
                                    MessageTimeout mt = messageQueue.get(0);
                                    messageQueue.remove(0);
                                    System.out.println("MessageQueue --");
                                    displayMessageMonitor(mt.getMessage(), mt.getTime());
                                }
                            }
                        },
                        ms + 1000
                );
            }
        }
    }
}
