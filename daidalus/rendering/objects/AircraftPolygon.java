/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.objects;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Polygon;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwindx.examples.daidalus.actions.DaidalusActions;
import gov.nasa.worldwindx.examples.daidalus.actions.View;
import java.util.ArrayList;

public class AircraftPolygon extends Polygon implements SelectListener {


    private String id;

    private String ownshipIconPath = "gov/nasa/worldwindx/examples/daidalus/data/ownship.png";
    private String introuderIconPath = "gov/nasa/worldwindx/examples/daidalus/data/introuder.png";

    private View view;

    public String getId() {
        return id;
    }

    public AircraftPolygon(String id, double latitude, double longitude, double altitude, Angle heading, View view){
        super();
        this.id = id;
        this.view = view;

        ShapeAttributes normalAttributes = new BasicShapeAttributes();

        double factor = 0.017;
        ArrayList<Position> pathPositions = new ArrayList<Position>();
//        pathPositions.add(Position.fromDegrees(latitude+(factor*2), longitude-factor, altitude));
//        pathPositions.add(Position.fromDegrees(latitude, longitude-factor, altitude));
//        pathPositions.add(Position.fromDegrees(latitude, longitude+factor, altitude));
//        pathPositions.add(Position.fromDegrees(latitude+(factor*2), longitude+factor, altitude));
//        pathPositions.add(Position.fromDegrees(latitude+(factor*2), longitude-factor, altitude));

        pathPositions.add(Position.fromDegrees(latitude+(factor*1), longitude-factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude-(factor*1), longitude-factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude-(factor*1), longitude+factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude + (factor * 1), longitude + factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude + (factor * 1), longitude - factor, altitude));

        this.setOuterBoundary(pathPositions);
        this.setValue(AVKey.DISPLAY_NAME, id);

        normalAttributes = new BasicShapeAttributes(normalAttributes);
        normalAttributes.setDrawInterior(true);
        normalAttributes.setDrawOutline(false);
//        normalAttributes.setInteriorMaterial(Material.WHITE);
        this.setAttributes(normalAttributes);

        float[] texCoords = new float[] {0, 0, 1, 0, 1, 1, 0, 1, 0, 0};
        if(id.equals("Ownship")){
            this.setTextureImageSource(ownshipIconPath, texCoords, 5);
//            System.out.println(">> Owns: " + latitude + ", " + longitude);
        }
        else{
            this.setTextureImageSource(introuderIconPath, texCoords, 5);
//            System.out.println(">> Intr: " + latitude + ", " + longitude);
        }
//        System.out.println(id + " angle: " + heading.degrees);
        this.setRotation(heading.degrees);
        this.view.getWwd().addSelectListener(this);
    }

    public void goTo(double latitude, double longitude, double altitude, Angle heading){
        double factor = 0.017;
        ArrayList<Position> pathPositions = new ArrayList<Position>();
        pathPositions.add(Position.fromDegrees(latitude+(factor*1), longitude-factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude-(factor*1), longitude-factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude-(factor*1), longitude+factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude+(factor*1), longitude+factor, altitude));
        pathPositions.add(Position.fromDegrees(latitude+(factor*1), longitude-factor, altitude));
        this.setOuterBoundary(pathPositions);
        this.setRotation(heading.degrees);
    }

    @Override
    public void selected(SelectEvent event) {
        if (event.getEventAction() == SelectEvent.LEFT_DOUBLE_CLICK && event.getTopObject() == this){
            DaidalusActions daidalusActions = view.getDaidalusActions();
            daidalusActions.setFollowingAircraft(this.id);
        }
    }

//    @Override
//    public void mouseClicked(MouseEvent e) {
//        System.out.println(id + " CLICKED!");
//    }
//
//    @Override
//    public void mousePressed(MouseEvent e) {
//        System.out.println(id + " CLICKED!");
//    }
//
//    @Override
//    public void mouseReleased(MouseEvent e) {
//        System.out.println(id + " CLICKED!");
//
//    }
//
//    @Override
//    public void mouseEntered(MouseEvent e) {
//        System.out.println(id + " CLICKED!");
//
//    }
//
//    @Override
//    public void mouseExited(MouseEvent e) {
//        System.out.println(id + " CLICKED!");
//
//    }
}



