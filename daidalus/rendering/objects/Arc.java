/**
 * @author Piergiuseppe Mallozzi
 * @date August 2015
 */

package gov.nasa.worldwindx.examples.daidalus.rendering.objects;

public class Arc {

    private double start;
    private double end;
    private String proxymity;

    /**
     *
     * @param start Radians
     * @param end Radians
     * @param proxymity NEAR, NONE, RECOVERY
     */
    public Arc(double start, double end, String proxymity) {
        this.start = start;
        this.end = end;
        this.proxymity = proxymity;
    }

    public double getStart() {
        return start;
    }

    public double getEnd() {
        return end;
    }

    public String getNear() {
        return proxymity;
    }
}